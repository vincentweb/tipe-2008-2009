﻿namespace gdiTests
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TimerRepaint = new System.Windows.Forms.Timer(this.components);
            this.posX = new System.Windows.Forms.Label();
            this.posY = new System.Windows.Forms.Label();
            this.function = new System.Windows.Forms.Label();
            this.speed = new System.Windows.Forms.Label();
            this.direction = new System.Windows.Forms.Label();
            this.clearPath = new System.Windows.Forms.Button();
            this.elaspedTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Xseek = new System.Windows.Forms.Label();
            this.Yseek = new System.Windows.Forms.Label();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.UpDownTick = new System.Windows.Forms.NumericUpDown();
            this.copyright = new System.Windows.Forms.Label();
            this.trackerDirection = new System.Windows.Forms.TrackBar();
            this.trackDist = new System.Windows.Forms.TrackBar();
            this.labelDirection = new System.Windows.Forms.Label();
            this.labelDistance = new System.Windows.Forms.Label();
            this.accelerationUpDown = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.maxSpeedUpDown = new System.Windows.Forms.NumericUpDown();
            this.displayLines = new System.Windows.Forms.CheckBox();
            this.collide = new System.Windows.Forms.CheckBox();
            this.checkBoxRadar = new System.Windows.Forms.CheckBox();
            this.pathUpDown = new System.Windows.Forms.NumericUpDown();
            this.path = new System.Windows.Forms.Label();
            this.labelKnown = new System.Windows.Forms.Label();
            this.trackKnown = new System.Windows.Forms.TrackBar();
            this.gomme = new System.Windows.Forms.Button();
            this.delBrain = new System.Windows.Forms.Button();
            this.addBrain = new System.Windows.Forms.Button();
            this.Pause = new System.Windows.Forms.Button();
            this.Read = new System.Windows.Forms.Button();
            this.step = new System.Windows.Forms.Button();
            this.addLine = new System.Windows.Forms.Button();
            this.addPoint = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.UpDownTick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackDist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accelerationUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxSpeedUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackKnown)).BeginInit();
            this.SuspendLayout();
            // 
            // TimerRepaint
            // 
            this.TimerRepaint.Enabled = true;
            this.TimerRepaint.Interval = 40;
            this.TimerRepaint.Tick += new System.EventHandler(this.TimerRepaint_Tick);
            // 
            // posX
            // 
            this.posX.AutoSize = true;
            this.posX.Location = new System.Drawing.Point(23, 477);
            this.posX.Name = "posX";
            this.posX.Size = new System.Drawing.Size(13, 13);
            this.posX.TabIndex = 1;
            this.posX.Text = "0";
            // 
            // posY
            // 
            this.posY.AutoSize = true;
            this.posY.Location = new System.Drawing.Point(23, 499);
            this.posY.Name = "posY";
            this.posY.Size = new System.Drawing.Size(13, 13);
            this.posY.TabIndex = 2;
            this.posY.Text = "0";
            // 
            // function
            // 
            this.function.AutoSize = true;
            this.function.Location = new System.Drawing.Point(5, 426);
            this.function.Name = "function";
            this.function.Size = new System.Drawing.Size(45, 13);
            this.function.TabIndex = 3;
            this.function.Text = "function";
            this.function.Click += new System.EventHandler(this.function_Click);
            // 
            // speed
            // 
            this.speed.AutoSize = true;
            this.speed.ForeColor = System.Drawing.Color.Blue;
            this.speed.Location = new System.Drawing.Point(106, 477);
            this.speed.Name = "speed";
            this.speed.Size = new System.Drawing.Size(13, 13);
            this.speed.TabIndex = 5;
            this.speed.Text = "0";
            // 
            // direction
            // 
            this.direction.AutoSize = true;
            this.direction.ForeColor = System.Drawing.Color.Blue;
            this.direction.Location = new System.Drawing.Point(106, 499);
            this.direction.Name = "direction";
            this.direction.Size = new System.Drawing.Size(13, 13);
            this.direction.TabIndex = 6;
            this.direction.Text = "0";
            // 
            // clearPath
            // 
            this.clearPath.Location = new System.Drawing.Point(192, 442);
            this.clearPath.Name = "clearPath";
            this.clearPath.Size = new System.Drawing.Size(69, 23);
            this.clearPath.TabIndex = 7;
            this.clearPath.Text = "Clear Path";
            this.clearPath.UseVisualStyleBackColor = true;
            this.clearPath.Click += new System.EventHandler(this.clearPath_Click);
            // 
            // elaspedTime
            // 
            this.elaspedTime.AutoSize = true;
            this.elaspedTime.Location = new System.Drawing.Point(684, 426);
            this.elaspedTime.Name = "elaspedTime";
            this.elaspedTime.Size = new System.Drawing.Size(13, 13);
            this.elaspedTime.TabIndex = 8;
            this.elaspedTime.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 477);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "x";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 499);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "y";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(64, 477);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "speed";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(64, 499);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "direct";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(646, 426);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "delay";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(139, 476);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Xseek";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(139, 499);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Yseek";
            // 
            // Xseek
            // 
            this.Xseek.AutoSize = true;
            this.Xseek.Location = new System.Drawing.Point(182, 476);
            this.Xseek.Name = "Xseek";
            this.Xseek.Size = new System.Drawing.Size(13, 13);
            this.Xseek.TabIndex = 16;
            this.Xseek.Text = "0";
            this.Xseek.Click += new System.EventHandler(this.Xseek_Click);
            // 
            // Yseek
            // 
            this.Yseek.AutoSize = true;
            this.Yseek.Location = new System.Drawing.Point(182, 499);
            this.Yseek.Name = "Yseek";
            this.Yseek.Size = new System.Drawing.Size(13, 13);
            this.Yseek.TabIndex = 17;
            this.Yseek.Text = "0";
            // 
            // comboBoxMode
            // 
            this.comboBoxMode.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBoxMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Items.AddRange(new object[] {
            "Motion Planning 1",
            "Motion Planning 2",
            "Speed regulator"});
            this.comboBoxMode.Location = new System.Drawing.Point(267, 442);
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.Size = new System.Drawing.Size(120, 21);
            this.comboBoxMode.TabIndex = 19;
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            // 
            // UpDownTick
            // 
            this.UpDownTick.Location = new System.Drawing.Point(255, 471);
            this.UpDownTick.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.UpDownTick.Name = "UpDownTick";
            this.UpDownTick.Size = new System.Drawing.Size(42, 20);
            this.UpDownTick.TabIndex = 20;
            this.UpDownTick.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.UpDownTick.ValueChanged += new System.EventHandler(this.UpDownTick_ValueChanged);
            // 
            // copyright
            // 
            this.copyright.AutoSize = true;
            this.copyright.BackColor = System.Drawing.SystemColors.Control;
            this.copyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyright.ForeColor = System.Drawing.Color.RoyalBlue;
            this.copyright.Location = new System.Drawing.Point(215, 503);
            this.copyright.Margin = new System.Windows.Forms.Padding(0);
            this.copyright.Name = "copyright";
            this.copyright.Size = new System.Drawing.Size(200, 13);
            this.copyright.TabIndex = 24;
            this.copyright.Text = "Motion Planning example by V. Angladon";
            // 
            // trackerDirection
            // 
            this.trackerDirection.Location = new System.Drawing.Point(603, 225);
            this.trackerDirection.Name = "trackerDirection";
            this.trackerDirection.Size = new System.Drawing.Size(104, 45);
            this.trackerDirection.TabIndex = 25;
            this.trackerDirection.Value = 8;
            this.trackerDirection.Scroll += new System.EventHandler(this.trackerDirection_Scroll);
            // 
            // trackDist
            // 
            this.trackDist.Location = new System.Drawing.Point(603, 285);
            this.trackDist.Name = "trackDist";
            this.trackDist.Size = new System.Drawing.Size(104, 45);
            this.trackDist.TabIndex = 27;
            this.trackDist.Value = 10;
            this.trackDist.Scroll += new System.EventHandler(this.trackDist_Scroll);
            // 
            // labelDirection
            // 
            this.labelDirection.Location = new System.Drawing.Point(624, 260);
            this.labelDirection.Name = "labelDirection";
            this.labelDirection.Size = new System.Drawing.Size(67, 18);
            this.labelDirection.TabIndex = 28;
            this.labelDirection.Text = "direction";
            this.labelDirection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDistance
            // 
            this.labelDistance.Location = new System.Drawing.Point(624, 317);
            this.labelDistance.Name = "labelDistance";
            this.labelDistance.Size = new System.Drawing.Size(67, 13);
            this.labelDistance.TabIndex = 30;
            this.labelDistance.Text = "distance";
            this.labelDistance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // accelerationUpDown
            // 
            this.accelerationUpDown.Location = new System.Drawing.Point(642, 498);
            this.accelerationUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.accelerationUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.accelerationUpDown.Name = "accelerationUpDown";
            this.accelerationUpDown.Size = new System.Drawing.Size(55, 20);
            this.accelerationUpDown.TabIndex = 31;
            this.accelerationUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.accelerationUpDown.ValueChanged += new System.EventHandler(this.accelerationBox_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(215, 475);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "refresh";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(572, 502);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "acceleration";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(575, 476);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "max speed";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // maxSpeedUpDown
            // 
            this.maxSpeedUpDown.Location = new System.Drawing.Point(642, 474);
            this.maxSpeedUpDown.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.maxSpeedUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.maxSpeedUpDown.Name = "maxSpeedUpDown";
            this.maxSpeedUpDown.Size = new System.Drawing.Size(55, 20);
            this.maxSpeedUpDown.TabIndex = 34;
            this.maxSpeedUpDown.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.maxSpeedUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // displayLines
            // 
            this.displayLines.AutoSize = true;
            this.displayLines.Location = new System.Drawing.Point(642, 347);
            this.displayLines.Name = "displayLines";
            this.displayLines.Size = new System.Drawing.Size(46, 17);
            this.displayLines.TabIndex = 38;
            this.displayLines.Text = "map";
            this.displayLines.UseVisualStyleBackColor = true;
            this.displayLines.CheckedChanged += new System.EventHandler(this.displayLines_CheckedChanged);
            // 
            // collide
            // 
            this.collide.AutoSize = true;
            this.collide.Checked = true;
            this.collide.CheckState = System.Windows.Forms.CheckState.Checked;
            this.collide.Location = new System.Drawing.Point(642, 370);
            this.collide.Name = "collide";
            this.collide.Size = new System.Drawing.Size(56, 17);
            this.collide.TabIndex = 39;
            this.collide.Text = "collide";
            this.collide.UseVisualStyleBackColor = true;
            this.collide.CheckedChanged += new System.EventHandler(this.collide_CheckedChanged);
            // 
            // checkBoxRadar
            // 
            this.checkBoxRadar.AutoSize = true;
            this.checkBoxRadar.Checked = true;
            this.checkBoxRadar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRadar.Location = new System.Drawing.Point(642, 393);
            this.checkBoxRadar.Name = "checkBoxRadar";
            this.checkBoxRadar.Size = new System.Drawing.Size(55, 17);
            this.checkBoxRadar.TabIndex = 40;
            this.checkBoxRadar.Text = "radars";
            this.checkBoxRadar.UseVisualStyleBackColor = true;
            this.checkBoxRadar.CheckedChanged += new System.EventHandler(this.checkBoxRadar_CheckedChanged);
            // 
            // pathUpDown
            // 
            this.pathUpDown.Location = new System.Drawing.Point(642, 448);
            this.pathUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.pathUpDown.Name = "pathUpDown";
            this.pathUpDown.Size = new System.Drawing.Size(55, 20);
            this.pathUpDown.TabIndex = 41;
            this.pathUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // path
            // 
            this.path.Location = new System.Drawing.Point(601, 450);
            this.path.Name = "path";
            this.path.Size = new System.Drawing.Size(45, 13);
            this.path.TabIndex = 42;
            this.path.Text = "path";
            this.path.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelKnown
            // 
            this.labelKnown.Location = new System.Drawing.Point(625, 195);
            this.labelKnown.Name = "labelKnown";
            this.labelKnown.Size = new System.Drawing.Size(67, 18);
            this.labelKnown.TabIndex = 44;
            this.labelKnown.Text = "known";
            this.labelKnown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackKnown
            // 
            this.trackKnown.Location = new System.Drawing.Point(604, 160);
            this.trackKnown.Name = "trackKnown";
            this.trackKnown.Size = new System.Drawing.Size(104, 45);
            this.trackKnown.TabIndex = 43;
            this.trackKnown.Value = 2;
            this.trackKnown.Scroll += new System.EventHandler(this.trackKnown_Scroll);
            // 
            // gomme
            // 
            this.gomme.Image = global::gdiTests.Properties.Resources.gomme;
            this.gomme.Location = new System.Drawing.Point(599, 421);
            this.gomme.Name = "gomme";
            this.gomme.Size = new System.Drawing.Size(22, 22);
            this.gomme.TabIndex = 45;
            this.gomme.UseVisualStyleBackColor = true;
            this.gomme.Click += new System.EventHandler(this.gomme_Click);
            // 
            // delBrain
            // 
            this.delBrain.Image = global::gdiTests.Properties.Resources.delMobile;
            this.delBrain.Location = new System.Drawing.Point(571, 421);
            this.delBrain.Name = "delBrain";
            this.delBrain.Size = new System.Drawing.Size(22, 22);
            this.delBrain.TabIndex = 37;
            this.delBrain.UseVisualStyleBackColor = true;
            this.delBrain.Click += new System.EventHandler(this.delBrain_Click);
            // 
            // addBrain
            // 
            this.addBrain.Image = global::gdiTests.Properties.Resources.addMobile;
            this.addBrain.Location = new System.Drawing.Point(543, 421);
            this.addBrain.Name = "addBrain";
            this.addBrain.Size = new System.Drawing.Size(22, 22);
            this.addBrain.TabIndex = 36;
            this.addBrain.UseVisualStyleBackColor = true;
            this.addBrain.Click += new System.EventHandler(this.addBrain_Click);
            // 
            // Pause
            // 
            this.Pause.Image = global::gdiTests.Properties.Resources.pause;
            this.Pause.Location = new System.Drawing.Point(363, 470);
            this.Pause.Name = "Pause";
            this.Pause.Size = new System.Drawing.Size(24, 23);
            this.Pause.TabIndex = 23;
            this.Pause.UseVisualStyleBackColor = true;
            this.Pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // Read
            // 
            this.Read.Image = global::gdiTests.Properties.Resources.read;
            this.Read.Location = new System.Drawing.Point(333, 470);
            this.Read.Name = "Read";
            this.Read.Size = new System.Drawing.Size(24, 23);
            this.Read.TabIndex = 22;
            this.Read.UseVisualStyleBackColor = true;
            this.Read.Click += new System.EventHandler(this.Read_Click);
            // 
            // step
            // 
            this.step.Image = global::gdiTests.Properties.Resources.step;
            this.step.Location = new System.Drawing.Point(303, 470);
            this.step.Name = "step";
            this.step.Size = new System.Drawing.Size(24, 23);
            this.step.TabIndex = 21;
            this.step.UseVisualStyleBackColor = true;
            this.step.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // addLine
            // 
            this.addLine.Image = global::gdiTests.Properties.Resources.newLine;
            this.addLine.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addLine.Location = new System.Drawing.Point(108, 442);
            this.addLine.Name = "addLine";
            this.addLine.Size = new System.Drawing.Size(77, 23);
            this.addLine.TabIndex = 4;
            this.addLine.Text = "     New Line";
            this.addLine.UseVisualStyleBackColor = true;
            this.addLine.Click += new System.EventHandler(this.addLine_Click_1);
            // 
            // addPoint
            // 
            this.addPoint.BackColor = System.Drawing.Color.Transparent;
            this.addPoint.FlatAppearance.BorderSize = 0;
            this.addPoint.Image = global::gdiTests.Properties.Resources.newPoint;
            this.addPoint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addPoint.Location = new System.Drawing.Point(1, 442);
            this.addPoint.Name = "addPoint";
            this.addPoint.Size = new System.Drawing.Size(101, 23);
            this.addPoint.TabIndex = 0;
            this.addPoint.Text = "    Add a point";
            this.addPoint.UseVisualStyleBackColor = false;
            this.addPoint.Click += new System.EventHandler(this.addPoint_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 526);
            this.Controls.Add(this.gomme);
            this.Controls.Add(this.labelKnown);
            this.Controls.Add(this.trackKnown);
            this.Controls.Add(this.pathUpDown);
            this.Controls.Add(this.path);
            this.Controls.Add(this.checkBoxRadar);
            this.Controls.Add(this.collide);
            this.Controls.Add(this.displayLines);
            this.Controls.Add(this.delBrain);
            this.Controls.Add(this.addBrain);
            this.Controls.Add(this.maxSpeedUpDown);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.accelerationUpDown);
            this.Controls.Add(this.labelDistance);
            this.Controls.Add(this.labelDirection);
            this.Controls.Add(this.trackDist);
            this.Controls.Add(this.trackerDirection);
            this.Controls.Add(this.copyright);
            this.Controls.Add(this.Pause);
            this.Controls.Add(this.Read);
            this.Controls.Add(this.step);
            this.Controls.Add(this.UpDownTick);
            this.Controls.Add(this.comboBoxMode);
            this.Controls.Add(this.Yseek);
            this.Controls.Add(this.Xseek);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.elaspedTime);
            this.Controls.Add(this.clearPath);
            this.Controls.Add(this.direction);
            this.Controls.Add(this.speed);
            this.Controls.Add(this.addLine);
            this.Controls.Add(this.function);
            this.Controls.Add(this.posY);
            this.Controls.Add(this.posX);
            this.Controls.Add(this.addPoint);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.RightToLeftLayout = true;
            this.Text = "MotionPlanning";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.up);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.down);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.move);
            ((System.ComponentModel.ISupportInitialize)(this.UpDownTick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackDist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accelerationUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxSpeedUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackKnown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addPoint;
        private System.Windows.Forms.Label posX;
        private System.Windows.Forms.Label posY;
        private System.Windows.Forms.Label function;
        private System.Windows.Forms.Button addLine;
        private System.Windows.Forms.Label speed;
        private System.Windows.Forms.Label direction;
        private System.Windows.Forms.Button clearPath;
        private System.Windows.Forms.Label elaspedTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Xseek;
        private System.Windows.Forms.Label Yseek;
        private System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.NumericUpDown UpDownTick;
        private System.Windows.Forms.Button step;
        private System.Windows.Forms.Button Read;
        private System.Windows.Forms.Button Pause;
        private System.Windows.Forms.Label copyright;
        private System.Windows.Forms.TrackBar trackerDirection;
        private System.Windows.Forms.TrackBar trackDist;
        private System.Windows.Forms.Label labelDirection;
        private System.Windows.Forms.Label labelDistance;
        private System.Windows.Forms.NumericUpDown accelerationUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown maxSpeedUpDown;
        private System.Windows.Forms.Button addBrain;
        private System.Windows.Forms.Button delBrain;
        private System.Windows.Forms.CheckBox displayLines;
        private System.Windows.Forms.CheckBox collide;
        private System.Windows.Forms.CheckBox checkBoxRadar;
        private System.Windows.Forms.NumericUpDown pathUpDown;
        private System.Windows.Forms.Label path;
        private System.Windows.Forms.Label labelKnown;
        private System.Windows.Forms.TrackBar trackKnown;
        private System.Windows.Forms.Button gomme;
    }
}

