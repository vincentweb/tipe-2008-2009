﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using geometry;
using MovingObject;
using dataStruct;
using dataStruct.map;
using System.Threading;

namespace gdiTests
{
    enum MOUSE_MODE	// mouse mode
    {
        NOTHING,
        DRAW_POINT,
        DRAW_LINE,
        MOVE_POINT,
        SELECT_PT1,
        SELECT_PT2,
    }
    enum MOBILE_MODE
    {
        MOTION1,
        MOTION2,
        REGULATOR,
        STEP,
        STOPPED
    }

    enum MOBILE_ACTION	// mouse mode
    {
        PLAY,
        STEP,
        STOPPED
    }

    public partial class Form1 : Form
    {
        public int x = 0;
        private int timestamp = 0;
        private int prevTime = 0;
        public  static int windowHeight = 420; // in fact mobile area !!
        public  static int windowWidth  = 600;
        private System.Windows.Forms.Timer TimerRepaint;
        private MOUSE_MODE    mouseMode    = MOUSE_MODE.NOTHING;
        private MOBILE_MODE   mobileMode   = MOBILE_MODE.MOTION2;
        private MOBILE_ACTION mobileAction = MOBILE_ACTION.PLAY;
        private Geometry      geo          = new Geometry();
        private LArray        map          = new LArray(windowWidth, windowHeight, 10);
        private PointL        movingPt     = new Point(-1, -1);
        private Line          newLine      = new Line(-1, -1, -1, -1);
        private BrainSelector brainSelector;
        private static PointLCollection pointList = new PointLCollection();
        private static LineCollection   lineList  = new LineCollection();
        private int iMovingPoint = -1;
        private bool showRadars = true;
        public  PointL goal = new PointL(300, 450, "Goal");
        PointL pi = new PointL(0, 0);

        private bool displayMap = false;
        
        public Form1()
        {
            InitializeComponent();
            brainSelector = new BrainSelector();
            //brainSelector.addBrain(40, 400, 5, windowHeight, windowWidth, 0, 0, 180, goal, map);
            //brainSelector.addBrain(11, 11, 5, windowHeight, windowWidth, 0, 180, 0, goal, map);
            brainSelector.addBrain(100, 100, 5, windowHeight, windowWidth, 180, 0, 0, goal, map);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true); //automatic double buffering
            pointList.Add(new PointL(10, 10, "i_A"));
            pointList.Add(new PointL(10, 400, "i_B"));
            pointList.Add(new PointL(400, 400, "i_C"));//400,500
            pointList.Add(new PointL(400, 10, "i_T"));
            pointList.Add(new PointL(200, 300, "d1"));
            pointList.Add(new PointL(300, 400, "d2"));
            pointList.Add(goal);
            Line l1 = new Line(pointList[0], pointList[1]);
            Line l2 = new Line(pointList[2], pointList[3]);
            Line l3 = new Line(pointList[0], pointList[3]);
            Line l4 = new Line(pointList[1], pointList[2]);
            Line l5 = new Line(pointList[4], pointList[5]);
            lineList.Add(l1);
            lineList.Add(l2);
            lineList.Add(l3);
            lineList.Add(l4);
            lineList.Add(l5);
            map.addLine(l1);
            map.addLine(l2);
            map.addLine(l3);
            map.addLine(l4);
            map.addLine(l5);
            calc();
        }

        private PointL findPointCoord(int x, int y)
        {
            foreach (PointL p in pointList)
            {
                if ((Math.Abs(p.x - x) < 3) && (Math.Abs(p.y - y) < 3))
                {
                    Form1.ActiveForm.Text = "found";
                    return p;
                }
            }
            Form1.ActiveForm.Text = "No point found";
            return new Point(-1, -1);
        }

        private int findIPointL(int x, int y)
        {
            int i = 0;
            foreach (PointL p in pointList)
            {
                if ((Math.Abs(p.x - x) < 3) && (Math.Abs(p.y - y) < 3))
                {
                    //Form1.ActiveForm.Text = "found";
                    return i;
                }
                i += 1;
            }
            //Form1.ActiveForm.Text = "No Ipoint found";
            return -1;
        }
        /// <summary>
        /// Get the point whose label is "label"
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        private PointL thePointL(string label)
        {
            foreach (PointL p in pointList)
            {
                if (p.label == label)
                {
                    //Form1.ActiveForm.Text = "found";
                    return p;
                }
            }
            Form1.ActiveForm.Text = "Nothing found";
            return new Point(-1, -1);
        }

        private void calc()
        {
            Point a = thePointL("i_A");
            Point b = thePointL("i_B");
            Point c = thePointL("i_C");
            Point t = thePointL("i_T");
            Vector ab = new Vector(a, b);
            Vector ac = new Vector(a, c);
            Vector at = new Vector(a, t);
            Line ab2 = new Line(a, b);
            Line ct2 = new Line(c, t);
            if (geo.intersectLine(ct2, ab2))
            {
                pi = geo.intersect(ct2, ab2);
                if (pi != null)
                {
                    function.Text = pi.x.ToString() + " : " + pi.y.ToString() + "  dist:" + Math.Round(geo.distance(pi.x, pi.y, c.Y, c.X),0).ToString();
                }
                //function.Text = geo.intersectLine(a,b,c,t).ToString();
            }
            else
            {
                pi = new PointL(0, 0);
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            Graphics g = pe.Graphics;
            
            Pen pn = new Pen(Color.LightGray, 100);
            //Rectangle rect = new Rectangle(50, 50, x + 50, 100);
            //g.DrawEllipse(pn, rect);
            Size size = new Size(1, 1);
            Size size2 = new Size(2, 2);
            Pen pen2 = new Pen(Color.Green);
            Pen pen3 = new Pen(Color.Red);
            Pen pen4 = new Pen(Color.LightBlue);
            Pen pen5 = new Pen(Color.Black);
            Pen pen6 = new Pen(Color.FromArgb(255,255,0));
            Pen penDB = new Pen(Color.DarkBlue,2F);
            Font font = new Font("Verdana", 10);
            Color BMPpenDB = Color.DarkBlue;
            try
            {
                g.DrawRectangle(penDB, new Rectangle(pi, size));
            }
            catch
            {
                Console.WriteLine("PB WITH PI");
            }
            g.DrawEllipse(pen5, 420, 420, 100, 100);
            
            brainSelector.paintAll(g);
            
            if (mobileMode != MOBILE_MODE.REGULATOR)
            {
                brainSelector.paintDestAll(g);
                if (showRadars == true)
                    brainSelector.paintMotionAll(g);
                //map.drawLines(g);
                if (displayMap)
                    map.drawCells(g);
                //g.DrawRectangle(pen5, new Rectangle(dest, size2));
                Line l;
                for (int i = 0; i < lineList.Count; i++)
                {
                    l = lineList[i];
                    g.DrawLine(pen2, l.p1, l.p2);
                }
            }

            PointL pt;
            for (int i = 0; i < pointList.Count; i++)
            {
                pt = pointList[i];
                g.DrawRectangle(pen2, new Rectangle(pt, size));
                g.DrawString(pt.label, Font, new SolidBrush(Color.Green), new PointF(pt.y, pt.x));
            }
            if (newLine.p2.x != -1)
                g.DrawLine(pen2, newLine.p1, newLine.p2);

            
            //if (movingPt != new Point(-1, -1))
            //{
            //    g.DrawRectangle(pen3, new Rectangle(movingPt, size));
            //    g.DrawString(movingPt.label, Font, new SolidBrush(Color.Red), new PointF(movingPt.x, movingPt.y));
            //}
        }


        //private void blink(object sender, MouseEventArgs e)
        //{
        //    this.posX.Text = "Blink";
        //    this.posY.Text = "Blink!";
        //}

        private void down(object sender, MouseEventArgs e)
        {
            switch (mouseMode)
            {
                case MOUSE_MODE.DRAW_POINT:
                    {
                        pointList.Add(new PointL(e.Y, e.X,5));
                        mouseMode = MOUSE_MODE.NOTHING;
                        break;
                    }
                case MOUSE_MODE.SELECT_PT1:
                    {
                        iMovingPoint = findIPointL(e.Y, e.X);
                        if (iMovingPoint != -1)
                        {
                            mouseMode = MOUSE_MODE.SELECT_PT2;
                            newLine.p1 = pointList[iMovingPoint];
                        }
                        break;
                    }
                    // Les lignes se déplacent avec les points car les lignes contiennent l'adresse physique des points (classe ~= pointeur)
                case MOUSE_MODE.SELECT_PT2:
                    {
                        iMovingPoint = findIPointL(e.Y, e.X);
                        if (iMovingPoint != -1)
                        {
                            mouseMode = MOUSE_MODE.NOTHING;
                            Line ll=new Line(newLine.p1, pointList[iMovingPoint]);
                            lineList.Add(ll);
                            map.addLine(ll);
                            //lineList.Add(new Line(newLine.p1.x,newLine.p1.y,newLine.p2.x,newLine.p2.y));
                            newLine.p2.x = -1;
                        }
                        mouseMode = MOUSE_MODE.NOTHING;
                        break;
                    }
                case MOUSE_MODE.NOTHING:
                    {
                        //movingPt = findPointCoord(e.X, e.Y);
                        iMovingPoint = findIPointL(e.Y, e.X); ;
                        if (iMovingPoint != -1)
                        {
                            //try
                            //{
                            //    //pointList.RemoveAt(pointList.IndexOf(movingPt));
                                mouseMode = MOUSE_MODE.MOVE_POINT;
                            //}
                            //catch
                            //{
                                //Form1.ActiveForm.Text = "ERROR";
                            //}
                            
                        }
                        if (brainSelector.findMobile(e.Y, e.X))
                        {
                            accelerationUpDown.Value = Math.Max(1,(((int)Math.Round((3 * ((Brain)brainSelector.getSelectedBrain()).m.getMaxAcceleration()),0))));
                            maxSpeedUpDown.Value     = ((int)((Brain)brainSelector.getSelectedBrain()).m.getMaxSpeed());
                            trackDist.Value          = ((int)((Brain)brainSelector.getSelectedBrain()).seeker.getMaxCoefDist() / 2);
                            trackerDirection.Value   = ((int)((Brain)brainSelector.getSelectedBrain()).seeker.getMaxCoefTheta() / 2);
                            trackKnown.Value         = ((int)((Brain)brainSelector.getSelectedBrain()).seeker.getMaxCoefKnown() / 2);
                        }
                        break;
                    }
                case MOUSE_MODE.MOVE_POINT:
                    {
                        //pointList.Add(movingPt);
                        //movingPt = new PointL(-1, -1);
                        Line line;
                        for (int i = 0; i < lineList.Count; i++)
                        {
                            line = lineList[i];
                            bool flag = false;
                            if (line.p1 == pointList[iMovingPoint])
                            {
                                map.moveLine(line);
                                flag = true;
                            }
                            if ( (line.p2 == pointList[iMovingPoint]) && (flag==false) )
                                map.moveLine(line);
                        }
                        iMovingPoint = -1;
                        mouseMode = MOUSE_MODE.NOTHING;
                        calc();
                        break;
                    }
            }
        }

        private void up(object sender, MouseEventArgs e)
        {
            //switch (mode)
            //{
            //    case MOUSE_MODE.MOVE_POINT:
            //        {

            //            break;
            //        }
            //}
        }

        private void move(object sender, MouseEventArgs e)
        {
            switch (mouseMode)
            {
                case MOUSE_MODE.MOVE_POINT:
                    {
                        if (e.Y<windowHeight)
                            pointList[iMovingPoint].x = e.Y;
                        if (e.X<windowWidth)
                            pointList[iMovingPoint].y = e.X;
                        //movingPt.x = e.X;
                        //movingPt.y = e.Y;
                        calc();
                        break;
                    }
                case MOUSE_MODE.SELECT_PT2:
                    {
                        if (e.Y < windowHeight)
                            newLine.p2.x = e.Y;
                        if (e.X < windowWidth)
                            newLine.p2.y = e.X;
                        break;
                    }
            }
        }


        //redessine le formulaire, à éviter
        private void TimerRepaint_Tick(object sender, System.EventArgs e)
        {

            //m.wander();
            
            if (mobileMode == MOBILE_MODE.REGULATOR)
            {
                brainSelector.regulatorAll(goal);
                int timestamp = Int32.Parse(DateTime.Now.ToString("mmssfff"));//game timestamp
                if (timestamp - prevTime > 60)
                {
                    prevTime = timestamp;
                    this.posX.Text = Math.Round(((Brain)(brainSelector.getSelectedBrain())).m.getDX(), 2).ToString();
                    this.posY.Text = Math.Round(((Brain)(brainSelector.getSelectedBrain())).m.getDY(), 2).ToString();
                    this.speed.Text = Math.Round(((Brain)(brainSelector.getSelectedBrain())).m.getSpeed(), 1).ToString();
                    this.direction.Text = Math.Round((((Brain)(brainSelector.getSelectedBrain())).m.getDirection() * 360 / 6.28319) % 360, 1).ToString();
                }
            }
            else //if (mobileMode == MOBILE_MODE.MOTION)
            {
                if ((mobileAction != MOBILE_ACTION.STOPPED)) //(mobileAction!=MOBILE_ACTION.STOPPED) to allow step  (mouseMode == MOUSE_MODE.NOTHING) && to stop the mobile during the move
                {
                    //((Brain)(brainSelector.getSelectedBrain())).motionPlaning();
                    brainSelector.motionAll(mobileMode);
                    if (mobileAction == MOBILE_ACTION.STEP)
                        mobileAction = MOBILE_ACTION.STOPPED;
                    
                    ////Console.WriteLine(Math.Round((((Brain)(brainSelector.getSelectedBrain())).m.getDirection() * 360 / 6.28319) % 360, 1).ToString());
                    timestamp = Int32.Parse(DateTime.Now.ToString("mmssfff"));//game timestamp
                    //// Print the track

                    if (timestamp - prevTime > 60)
                    {
                        prevTime = timestamp;
                        this.Xseek.Text = ((Brain)(brainSelector.getSelectedBrain())).dest.x.ToString();
                        this.Yseek.Text = ((Brain)(brainSelector.getSelectedBrain())).dest.y.ToString();
                        this.posX.Text = Math.Round(((Brain)(brainSelector.getSelectedBrain())).m.getDX(), 2).ToString();
                        this.posY.Text = Math.Round(((Brain)(brainSelector.getSelectedBrain())).m.getDY(), 2).ToString();
                        this.speed.Text = Math.Round(((Brain)(brainSelector.getSelectedBrain())).m.getSpeed(), 1).ToString();
                        this.direction.Text = Math.Round((((Brain)(brainSelector.getSelectedBrain())).m.getDirection() * 360 / 6.28319) % 360, 1).ToString();
                        long timing = ((Brain)(brainSelector.getSelectedBrain())).seeker.getElaspedTime();
                        if (timing != -1)
                            elaspedTime.Text = timing.ToString();
                    }
                }
            }
            this.Invalidate();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void addPoint_Click(object sender, EventArgs e)
        {
            mouseMode = MOUSE_MODE.DRAW_POINT;
        }

        private void addLine_Click(object sender, EventArgs e)
        {
            mouseMode = MOUSE_MODE.SELECT_PT1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mouseMode = MOUSE_MODE.SELECT_PT1;
        }

        private void addLine_Click_1(object sender, EventArgs e)
        {
            mouseMode = MOUSE_MODE.SELECT_PT1;
        }

        private void clearPath_Click(object sender, EventArgs e)
        {
            ((Brain)(brainSelector.getSelectedBrain())).clearPath();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Xseek_Click(object sender, EventArgs e)
        {

        }

        private void function_Click(object sender, EventArgs e)
        {

        }

        

        private void comboBoxMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxMode.SelectedItem.ToString())
            {
                case "Motion Planning 1":
                    {
                        mobileMode = MOBILE_MODE.MOTION1;
                        trackerDirection.Hide();
                        trackDist.Hide();
                        labelDirection.Hide();
                        labelDistance.Hide();
                        labelKnown.Hide();
                        trackKnown.Hide();
                        gomme.Hide();
                        break;
                    }
                case "Motion Planning 2":
                    {
                        mobileMode = MOBILE_MODE.MOTION2;
                        trackDist.Visible = true;
                        trackerDirection.Visible = true;
                        labelDistance.Visible = true;
                        labelDirection.Visible = true;
                        labelKnown.Visible = true;
                        trackKnown.Visible = true;
                        gomme.Visible = true;
                        break;
                    }
                case "Speed regulator":
                    {
                        mobileMode = MOBILE_MODE.REGULATOR;
                        trackerDirection.Hide();
                        trackDist.Hide();
                        labelDirection.Hide();
                        labelDistance.Hide();
                        labelKnown.Hide();
                        trackKnown.Hide();
                        gomme.Hide();
                        break;
                    }
            }
        }

        private void UpDownTick_ValueChanged(object sender, EventArgs e)
        {
            this.TimerRepaint.Interval=(int)UpDownTick.Value;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            mobileAction = MOBILE_ACTION.STEP;
        }

        private void Read_Click(object sender, EventArgs e)
        {
            mobileAction = MOBILE_ACTION.PLAY;
        }

        private void Pause_Click(object sender, EventArgs e)
        {
            mobileAction = MOBILE_ACTION.STOPPED;
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void accelerationBox_ValueChanged(object sender, EventArgs e)
        {
            ((Brain)(brainSelector.getSelectedBrain())).m.setAcceleration((double)accelerationUpDown.Value / 3);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            ((Brain)(brainSelector.getSelectedBrain())).m.setMaxSpeed((double)maxSpeedUpDown.Value);
        }

        private void trackerDirection_Scroll(object sender, EventArgs e)
        {
            ((Brain)(brainSelector.getSelectedBrain())).seeker.setMaxCoefTheta((double)trackerDirection.Value);
        }

        private void trackDist_Scroll(object sender, EventArgs e)
        {
            ((Brain)(brainSelector.getSelectedBrain())).seeker.setMaxCoefDist((double)trackDist.Value);
        }

        private void addBrain_Click(object sender, EventArgs e)
        {
            brainSelector.addBrain(45, 45, 5, windowHeight, windowWidth, goal, map);
        }

        private void delBrain_Click(object sender, EventArgs e)
        {
            brainSelector.deletedBrain();
        }

        private void displayLines_CheckedChanged(object sender, EventArgs e)
        {
            displayMap = displayLines.Checked;
        }

        private void collide_CheckedChanged(object sender, EventArgs e)
        {
            brainSelector.setCollide(collide.Checked);
        }

        private void checkBoxRadar_CheckedChanged(object sender, EventArgs e)
        {
            showRadars = checkBoxRadar.Checked;
        }

        private void trackKnown_Scroll(object sender, EventArgs e)
        {
            ((Brain)(brainSelector.getSelectedBrain())).seeker.setMaxCoefKnown((double)trackKnown.Value);
        }

        private void gomme_Click(object sender, EventArgs e)
        {
            ((Brain)(brainSelector.getSelectedBrain())).seeker.eraseMapKnown();
            ((Brain)(brainSelector.getSelectedBrain())).clearBmpMapKnown();
        }

    }
}
