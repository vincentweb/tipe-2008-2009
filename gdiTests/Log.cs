﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace gdiTests
{
    public class Log
    {
        private string filename;

        public Log(string filename)
        {
            this.filename = filename;
        }

        public void add(string message)
        {
            try
            {
                File.AppendAllText(filename, message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to add.");
            }
        }

        public void write(string message)
        {
            DateTime d1 = DateTime.Now;
            try
            {
                File.AppendAllText(filename, "\r\n" + d1.ToString("dd/MM HH:mm") + "   " + message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to write.");
            }
        }
    }
}
