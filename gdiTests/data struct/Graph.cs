﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace dataStruct
{
    class Graph
    {
        public Dictionary<double, GraphContainer> graph =
            new Dictionary<double, GraphContainer>();

        public Graph()
        {
        }
        
        public double antecedant(double image)
        {//for bijective graph
            foreach (KeyValuePair<double, GraphContainer> im in graph)
            {
                if (im.Value.dist == image)
                    return im.Key;
            }
            return -1;
        }
    }
}
