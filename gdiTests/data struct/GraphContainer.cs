﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dataStruct
{
    class GraphContainer
    {
        public double dist     = 0;
        public double coef     = 0;
        //public bool   known    = false;
        //public bool   overDist = false;

        public GraphContainer(double dist, double coef)
        {
            this.dist     = dist;
            this.coef     = coef;
            //this.known    = known;
            //this.overDist = overDist;
        }

    }
}
