﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using geometry;
using System.Collections;
using System.Drawing;

namespace dataStruct
{

    public class LineCollection : CollectionBase
    {
        public LineCollection() : base()
        {
        }
        public void Add(Line l)
        {
            this.List.Add(l);
        }
        public Line this[int index]
        {
            get 
            {
                if ((index >= 0) && (index < this.List.Count))
                    return (Line)List[index];
                else
                {
                    Console.WriteLine("INDEX PB!!!!!!!!!");
                    return null;
                }
            }
            set { List[index] = value; }
        }

        public int IndexOf(Line l)
        {
            int i = 0;
            foreach (Line line in this.List)
            {
                if (l == line)
                    return i;
                i += 1;
            }
            return -1;
        }

        public void Remove(Line l)
        {
            if (this.List.Count > 0)
            {
                int i = 0;
                foreach (Line line in this.List)
                {
                    if (l == line)
                    {
                        this.RemoveAt(i);
                        break;
                    }
                    i += 1;
                }
            }
        }
    }

    public class PointLCollection : CollectionBase
    {
        public PointLCollection()
            : base()
        {
        }
        int lastIndex = 0;
        public void Add(PointL p)
        {
            this.List.Add(p);
        }

        public void Add2(PointL p, int maxLength)
        {// liste circulaire
            if (this.List.Count < maxLength)
            {
                this.List.Add(p);
                lastIndex = this.List.Count;
            }
            else
            {
                this.List.RemoveAt(lastIndex % maxLength);
                this.List.Add(p);
            }
        }

        public PointL this[int index]
        {
            get { return (PointL)List[index]; }
            set { List[index] = value; }
        }

        public int IndexOf(PointL p)
        {
            int i=0;
            foreach (PointL pt in this.List)
            {
                if (p == pt)
                    return i;
                i += 1;
            }
            return -1;
        }

        public PointL getLast()
        {
            return (PointL)this.List[lastIndex-1];
        }

        public PointL getLastM()
        {// retourne l'avant-dernier
            if (lastIndex - 2 > 0)
                return (PointL)this.List[lastIndex - 2];
            else
                return (PointL)this.List[lastIndex - 1];
        }


    }
    
}
