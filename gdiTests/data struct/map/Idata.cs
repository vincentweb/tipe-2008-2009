﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using geometry;

namespace dataStruct.map
{
    interface Idata
    {
        void           addLine   (Line  l);
        LineCollection linesIn   (Point p);
        void           delLine   (Line l);
        void           drawCells (Graphics g);
        void           drawLines (Graphics g);
        void           moveLine  (Line l);
    }
}
