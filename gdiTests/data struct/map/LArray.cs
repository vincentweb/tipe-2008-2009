﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using geometry;
using dataStruct;
using dataStruct.map;

namespace dataStruct.map
{
    class LArray : Idata
    {
        LineCollection[,] array;
        //string name = "LArray"; // name of the Data struct
        int mapWidth;
        int mapHeight;
        int cellSize;
        int windowHeight;
        int windowWidth;
        LineToEq lineToEq = new LineToEq();
        Geometry geo = new Geometry();
        
        public LArray(int windowWidth, int windowHeight, int precision)
        {
            //precision  = cell size
            this.windowHeight = windowHeight;
            this.windowWidth  = windowWidth;
            mapHeight    = (int)Math.Ceiling(((double)windowHeight) / ((double)precision));
            mapWidth     = (int)Math.Ceiling((double)windowWidth / (double)precision);
            array        = new LineCollection[mapHeight, mapWidth];
            cellSize     = precision;
            for (int i = 0; i < mapHeight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    array[i, j] = new LineCollection();
                }
            }
        }


        public int getMapHeight()
        {
            return mapHeight;
        }
        public int getMapWidth()
        {
            return mapWidth;
        }
        public int getCellSize()
        {
            return cellSize;
        }

        public void addLine(Line l)
        {
            //Coordinates of the cells of the 2 points of the line
            int cx0 = l.p1.x / cellSize;
            int cy0 = l.p1.y / cellSize;
            int cxf = l.p2.x / cellSize;
            int cyf = l.p2.y / cellSize;
            int tf = Math.Max(Math.Abs(cyf-cy0), Math.Abs(cxf-cx0));
            // y(t) = ay * t + by
            // x(t) = ax * t + bx
            // y(0)  -> p1
            // y(tf) -> p2
            float bx = l.p1.x;
            float by = l.p1.y;
            float ax = ((float)(l.p2.x - bx)) / tf;
            float ay = ((float)(l.p2.y - by)) / tf;
            l.tf = tf;
            l.ax = ax;
            l.ay = ay;
            l.bx = bx;
            l.by = by;
            int prevpx = cx0; // previous point
            int prevpy = cy0;
            if ((prevpx<mapHeight)&&(prevpy<mapWidth))
                array[prevpx,prevpy].Add(l);
            int cxt;
            int cyt;
            for (float t = 1; t <= tf; t += 1)
            {
                cxt = (int)(ax * t + bx)/cellSize;
                cyt = (int)(ay * t + by)/cellSize;
                //We don't add 2 points in the same cell!
                if ((cxt != prevpx) || (cyt != prevpy))
                {
                    if ((cxt < mapHeight) && (cyt < mapWidth))
                        array[cxt, cyt].Add(l);
                    //Console.WriteLine("({0},{1})", cxt, cyt);
                }
                if ((Math.Abs(cxt - prevpx) + Math.Abs(cyt - prevpy)) > 1)
                {
                    int cxtd = (int)(ax * (t - .5F) + bx) / cellSize;
                    int cytd = (int)(ay * (t - .5F) + by) / cellSize;
                    if (((cxtd != prevpx) || (cytd != prevpy)) && ((cxtd != cxt) || (cytd != cyt)))
                    {
                        if ((cxtd < mapHeight) && (cytd < mapWidth))
                            array[cxtd, cytd].Add(l);
                        //Console.WriteLine("(({0},{1}))", cxtd, cytd);
                    }
                    else
                    {
                        if ((prevpx < mapHeight) && (cyt < mapWidth))
                            array[prevpx, cyt].Add(l);
                        //Console.WriteLine("[{0},{1}]", prevpx, cyt);
                    }
                }
                prevpx = cxt;
                prevpy = cyt;
            }
        }

        public void delLine(Line l)
        {
            for (int i = 0; i < mapHeight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    array[i, j].Remove(l);
                }
            }
        }

        public void moveLine(Line l)
        {
            delLine(l);
            addLine(l);
        }

        public LineCollection linesIn(Point p)
        {
            return array[p.X/cellSize, p.Y/cellSize];
        }

        public void drawLines(Graphics g)
        {
            Pen pn = new Pen(Color.LightCyan, 1);
            for (int i = 0; i <= windowWidth; i+=cellSize)
            {
                for (int j = 0; j <= windowHeight; j+=cellSize)
                {
                    g.DrawLine(pn, i, 0, i, windowHeight);
                    g.DrawLine(pn, 0, j, windowWidth, j);
                }
            }

        }

        public void drawCells(Graphics g)
        {
            Pen pn = new Pen(Color.LightCoral, 1);
            for (int i = 0; i < mapHeight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    if (array[i, j].Count > 0)
                    {
                        g.DrawRectangle(pn, j * cellSize, i * cellSize, cellSize, cellSize);
                    }
                }
            }
        }
        /// <summary>
        /// if  a position is free returns (-1,-1), else returns the cell which was just before the intersection
        /// </summary>
        /// <param name="l">line to test</param>
        /// <param name="x">mobile x</param>
        /// <param name="y">mobile y</param>
        /// <returns>Returns the cell before the intersection, if no intersection, returns Point(-1,-1)</returns>
        public PointL lineIntersect(Line l, int x, int y, Line mLine)
        {
            lineToEq.getEq(l, cellSize);
            PointL p = new PointL(-1, -1);
            int cxt     = x/cellSize;
            int cyt     = y/cellSize; // mobiles's cell
            int prevcxt = cxt;
            int prevcyt = cyt;
            for (float t = 0; t <= l.tf; t += .5F)//t=1
            {
                prevcxt = cxt;
                prevcyt = cyt;
                cxt = (int)(l.ax * t + l.bx) / cellSize;
                cyt = (int)(l.ay * t + l.by) / cellSize;
                //TODO:  TO CHANGE!!!!!!!!!!!!!!!!
                if ((cxt < 0) || (cyt < 0) || (cxt >= mapHeight) || (cyt >= mapWidth))
                    break;
                //We don't add 2 points in the same cell!
                if ((array[cxt, cyt].Count != 0) && (array[cxt, cyt].IndexOf(mLine)==-1) )
                {
                    p.x = prevcxt;
                    p.y = prevcyt;
                    return p;
                }
                
            }
            return p;
        }
        /// <summary>
        /// return the distance from the closest line
        /// </summary>
        /// <param name="l">line to test </param>
        /// <param name="x">mobile x</param>
        /// <param name="y">mobile y</param>
        /// <param name="mLine">the line of the mobile used to avoid collisions</param>
        /// <returns>distance</returns>
        public double lineDist(Line l, double x, double y, ref bool[,] mapKnown, double maxDistance, Line mLine)
        {
            lineToEq.getEq(l, cellSize); // TODO: lineToEq.smth to avoid!! else confusion between lines of 2 mobiles
            PointL p    = new PointL(-1, -1);
            int cxt     = (int)x / cellSize;
            int cyt     = (int)y / cellSize;
            int prevcxt = cxt;
            int prevcyt = cyt;
            double dist = maxDistance+1;
            double d    = 0;
            for (float t = 0; t <= l.tf; t += .5F)//t=.5F
            {
                prevcxt = cxt;
                prevcyt = cyt;
                cxt = (int)(l.ax * t + l.bx) / cellSize;
                cyt = (int)(l.ay * t + l.by) / cellSize;
                
                //TODO:  TO CHANGE!!!!!!!!!!!!!!!!
                if ((cxt < 0) || (cyt < 0) || (cxt >= mapHeight) || (cyt >= mapWidth))
                {
                    //Console.WriteLine(">>> Out");
                    if (geo.intersectLine(l, new Line(0,0,mapWidth*cellSize, 0)))
                        return geo.distance(new PointL(x,y),geo.intersect(l, new Line(0,0,mapWidth*cellSize, 0)));// Distance to the top line
                    else if (geo.intersectLine(l, new Line(0, 0, 0, mapHeight * cellSize)))
                        return geo.distance(new PointL(x, y), geo.intersect(l, new Line(0, 0, 0, mapHeight * cellSize)));// Distance to the left line
                    else if (geo.intersectLine(l, new Line(mapWidth * cellSize, 0, mapWidth*cellSize, mapHeight * cellSize)))
                        return geo.distance(new PointL(x, y), geo.intersect(l, new Line(mapWidth * cellSize, 0, mapWidth * cellSize, mapHeight * cellSize)));// Distance to the right line
                    else if (geo.intersectLine(l, new Line(0, mapHeight * cellSize, mapWidth * cellSize, mapHeight * cellSize)))
                        return geo.distance(new PointL(x, y), geo.intersect(l, new Line(0, mapHeight * cellSize, mapWidth * cellSize, mapHeight * cellSize)));// Distance to the bottom line
                    else
                        return 0;
                }
                //We don't add 2 points in the same cell!
                //Console.Write(".({0},{1}).",cxt,cyt);
                if (array[cxt, cyt].Count != 0)
                {
                    //Console.WriteLine("sth");
                    Line line;
                    for (int i = 0; i < array[prevcxt, cyt].Count; i++)
                    {
                        line = array[prevcxt, cyt][i];
                        if ((geo.intersectLine(l, line)) && (line != mLine))// integers only?!?!?!?!!!!
                        {
                            PointL pi = geo.intersect(l, line);
                            if (pi != null)
                            {
                                d = geo.distance(pi, new PointL(x, y));
                                //if (dist == 0)
                                //    dist = d;
                                //else
                                dist = Math.Min(dist, d);
                            }
                            //else
                            //    Console.WriteLine("INTERSECT PB !!!!!!!!!!");
                        }
                    }
                    if (dist < maxDistance + 1)
                        return dist;// we don't want to see behind the wall so we break with a return
                }
                if (array[prevcxt, cyt].Count != 0)
                {
                    //Console.WriteLine("sth");
                    //TODO: Lock THIS
                    Line line;
                    for (int i = 0; i < array[prevcxt, cyt].Count; i++ )
                    {
                         line = array[prevcxt, cyt][i];
                        if ((geo.intersectLine(l, line)) && (line != mLine)) // integers only?!?!?!?!!!!
                        {
                            PointL pi = geo.intersect(l, line);
                            if (pi != null)
                            {
                                d = geo.distance(pi, new PointL(x, y));
                                //if (dist == 0)
                                //    dist = d;
                                //else
                                dist = Math.Min(dist, d);
                            }
                            //else
                            //    Console.WriteLine("INTERSECT PB !!!!!!!!!!");
                        }
                    }
                    if (dist < maxDistance + 1)
                        return dist;// we don't want to see behind the wall so we break with a return
                }
                if (array[cxt, prevcyt].Count != 0)
                {
                    //Console.WriteLine("sth");
                    Line line;
                    for (int i = 0; i < array[prevcxt, cyt].Count; i++)
                    {
                        line = array[prevcxt, cyt][i];
                        if ((geo.intersectLine(l, line)) && (line != mLine)) // integers only?!?!?!?!!!!
                        {
                            PointL pi = geo.intersect(l, line);
                            if (pi != null)
                            {
                                d = geo.distance(pi, new PointL(x, y));
                                //if (dist == 0)
                                //    dist = d;
                                //else
                                dist = Math.Min(dist, d);
                            }
                            //else
                            //    Console.WriteLine("INTERSECT PB !!!!!!!!!!");
                        }
                    }
                    if (dist < maxDistance + 1)
                        return dist;// we don't want to see behind the wall so we break with a return
                }
                if (t <= l.tf-4)
                    mapKnown[cyt, cxt] = true;// there were no intersect, thought there were a line in the cell
                //if (cyt >= 40)
                //    Console.WriteLine("Info... ({0};{1}) m:({2};{3}) lk4:({4},{5}) prev:({6},{7})", cxt, cyt, x, y, l.p2.x, l.p2.y, prevcxt, prevcyt);
                //if (cyt == 42)
                //    Console.WriteLine("too far");
            }
            return maxDistance + 1;
        }

    }
}
