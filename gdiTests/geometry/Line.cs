﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using geometry;
using System.Collections;
using System.Drawing;

namespace geometry
{

    public class Line
    {
        public PointL p1;
        public PointL p2;
        public double ax;
        public double ay;
        public double bx;
        public double by;
        public double tf;
        LineToEq lineToeq = new LineToEq();

        public Line(PointL p1, PointL p2)
        {
            this.p1 = p1;
            this.p2 = p2;
            lineToeq.getEq(p1, p2,10);// CellSIze ici
            getCoef();
        }
        public Line(int x1, int y1, int x2, int y2)
        {
            this.p1 = new PointL(x1, y1);
            this.p2 = new PointL(x2, y2);
            lineToeq.getEq(x1, y1, x2, y2, 1);//????
            getCoef();
        }

        public Line(double x1, double y1, double x2, double y2)
        {
            this.p1 = new PointL(x1, y1);
            this.p2 = new PointL(x2, y2);
            lineToeq.getEq(p1, p2, 1);//???? TODO: weird
            getCoef();
        }

        public bool Equals(Line l)
        {
            // If parameter is null return false:
            if ((object)l == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (p1.dx == l.p1.dx) && (p1.dy == l.p1.dy) && (p2.dx == l.p2.dx) && (p2.dy == l.p2.dy);
        }

        public static bool operator ==(Line l1, Line l2)
        {
            //// If both are null, or both are same instance, return true.
            //if (System.Object.ReferenceEquals(l1, l2))
            //{
            //    return true;
            //}

            // If one is null, but not both, return false.
            if (((object)l1 == null) || ((object)l2 == null))
            {
                return false;
            }

            // Return true if the fields match:

            return l1.p1.dx == l2.p1.dx && l1.p2.dx == l2.p2.dx && l1.p1.dy == l2.p1.dy && l1.p2.dy == l2.p2.dy;
        }

        public static bool operator !=(Line l1, Line l2)
        {
            return !(l1 == l2);
        }



        private void getCoef()
        {
            ax = lineToeq.ax;
            ay = lineToeq.ay;
            bx = lineToeq.bx;
            by = lineToeq.by;
            tf = lineToeq.tf;
        }
    }

 

    public struct Vector
    {
        public Vector(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public Vector(PointL a, PointL b)
        {
            x = b.x - a.x;
            y = b.y - a.y;
        }

        public int x;
        public int y;

    }

    public struct VectorD
    {
        public double x;
        public double y;

        public VectorD(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
        public VectorD(PointL a, PointL b)
        {
            x = b.x - a.x;
            y = b.y - a.y;
        }

        public static VectorD operator *(double scal, VectorD a)
        {
            a.x *= scal;
            a.y *= scal;
            return a;
        }

        public static double operator *(VectorD a, VectorD b)
        {
            return a.x * b.x + a.y * b.y;
        }

    }
}