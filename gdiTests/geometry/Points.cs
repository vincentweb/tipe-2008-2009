﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using gdiTests;
using dataStruct;

namespace geometry
{
    public class Doublet
    {
        public object x;
        public object y;
        public Doublet(object x, object y)
        {
            this.x = x;
            this.y = y;
        }
    }
    public class PointL
    {
        public int    x  = -8764359;
        public int    y  = -1863423;
        public double dx = -55.1268;
        public double dy = -3197.35;
        public string type = "unknown";
        //public Point p;
        public string label;
        public PointLCollection lineWith;

        public PointL(int x, int y, string label)
        {
            this.x = x;
            this.y = y;
            this.dx = x;
            this.dy = y;
            type = "int";
            //p = new Point(x, y);
            this.label = label;
        }

        public PointL(double x, double y, string label)
        {
            this.dx = x;
            this.dy = y;
            this.x = (int)Math.Round(x,0);
            this.y = (int)Math.Round(y, 0);
            //p = new Point(x, y);
            this.label = label;
            type = "double";
        }

        public PointL(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.dx = x;
            this.dy = y;
            type = "int";
            //p = new Point(x, y);
            Random r = new Random();
            this.label = Convert.ToChar(r.Next() % 26 + 65).ToString();
        }

        public PointL(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.dx = x;
            this.dy = y;
            //p = new Point(x, y);
            this.label = "";
            type = "int";
        }

        public PointL(double x, double y)
        {
            this.dx = x;
            this.dy = y;
            this.x = (int)Math.Round(x, 0);
            this.y = (int)Math.Round(y, 0);
            //p = new Point(x, y);
            this.label = "";
            type = "double";
        }

        public Doublet getPoint()
        {
            if (type=="int")
                return new Doublet(x, y);
            else if (type=="double")
                return new Doublet(dx, dy);
            else
                return new Doublet(-10, -10);
        }

        public static implicit operator PointF(PointL pt)
        {
                return new PointF((float)pt.dy, (float)pt.dx);
        }

        public static implicit operator Point(PointL pt)
        {
            if (pt != null)
                return new Point(pt.y, pt.x);
            else
            {
                Console.WriteLine("--- Exception int PointL.implicit pointL->point");
                return new Point(-1, -1);
            }
        }

        public static implicit operator PointL(PointF pt)
        {
            return new PointL(pt.Y, pt.X);
        }

        public static implicit operator PointL(Point pt)
        {
            return new PointL(pt.Y, pt.X);
        }

    }

    //public class Points
    //{
    //    public int x;
    //    public int y;
    //    public Points nextPt;
        
    //    public Points(int x, int y)
    //    {
    //        this.x = x;
    //        this.y = y;
    //        nextPt = null;
    //    }
    //    public Points(int x, int y, Points nextPt)
    //    {
    //        this.x = x;
    //        this.y = y;
    //        this.nextPt = nextPt;
    //    }
    //}


}
