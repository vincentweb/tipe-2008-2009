﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using gdiTests;

namespace geometry
{
    class LineToEq
    {
        public int cx0;
        public int cy0;
        public int cxf;
        public int cyf;
        public int tf;
        public float ax;
        public float bx;
        public float ay;
        public float by;
        
        public void getEq(int x1, int y1, int x2, int y2, int cellSize)
        {
            cx0 = x1 / cellSize;
            cy0 = y1 / cellSize;
            cxf = x2 / cellSize;
            cyf = y2 / cellSize;
            //if ((cxf < 0) || (cyf < 0))
            //    Console.WriteLine("Impossible");
            tf = Math.Max(Math.Abs(cyf - cy0), Math.Abs(cxf - cx0));
            // y(t) = ay * t + by
            // x(t) = ax * t + bx
            // y(0)  -> p1
            // y(tf) -> p2
            bx = x1;
            by = y1;
            ax = ((float)(x2 - bx)) / tf;
            ay = ((float)(y2 - by)) / tf;
        }

        public void getEq(Line l, int cellSize)
        {
            getEq(l.p1.x, l.p1.y, l.p2.x, l.p2.y, cellSize);
        }
        public void getEq(PointL p1, PointL p2, int cellSize)
        {
            getEq(p1.x, p1.y, p2.x, p2.y, cellSize);
        }
    }
}
