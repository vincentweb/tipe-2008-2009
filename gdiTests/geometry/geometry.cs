// geometry.cs created with MonoDevelop
// User: vincentweb at 21:22 25/10/2008
//

using System;
using System.Drawing;
using gdiTests;

namespace geometry
{

	public partial class Geometry
	{
		
		public Geometry()
		{
		}
		
		public int scalProd(Vector a, Vector b)
		{
			return a.x*b.x+a.y*b.y;
		}
        public double scalProd(VectorD a, VectorD b)
        {
            return a.x * b.x + a.y * b.y;
        }
		public int orientation(PointL o, PointL a, PointL b)
		{
			Vector oa2=new Vector(-a.y+o.y,a.x-o.x);
			Vector ob=new Vector(o,b);
			int prod=scalProd(oa2,ob);
			if (prod>0)
			{
				return 1;
			}
			else if(prod==0)
			{
				return 0;
			}
			else
			{
				return -1;
			}
		}

        public double norme2(Vector a)
        {
            return a.x * a.x + a.y * a.y;
        }

        public double norme2(VectorD a)
        {
            return a.x * a.x + a.y * a.y;
        }

        public double distance(Point p1, Point p2)
        {
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        public double distance(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        public double angle(Point p1, Point p2)
        {
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            return Math.Atan2(dy, dx);
        }

        public double angle(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            double angle = Math.Atan2(dy, dx);
            if (angle < 0)
                angle += 6.28319;
            return angle;
        }

        

        public double distPtLine(PointL p, Line l)
        {
            VectorD ab = new VectorD(l.p1, l.p2);
            VectorD ap = new VectorD(p, l.p1);
            return Math.Sqrt(norme2(ap)-norme2(scalProd(ap,ab)/norme2(ab)*ab));
        }

		public int orientation(PointL a, Vector b, Vector o)
		{
			Vector oa2=new Vector(-a.y+o.y,a.x-o.x);
			Vector ob=b;
			int prod=scalProd(oa2,ob);
			if (prod>0)
			{
				return 1;
			}
			else if(prod==0)
			{
				return 0;
			}
			else
			{
				return -1;
			}
		}
		
		public bool in_sector(Vector ab, Vector cd, Vector at)
		{
			PointL o =new PointL(0,0);
            if (orientation(o,ab,cd)>=0)
			    return (orientation(o,ab,at)>=0)&&(orientation(o,at,cd)>=0);
            else
                return !((orientation(o, cd, at) >= 0) && (orientation(o, at, ab) >= 0));
		}
		/// <summary>
		///  To know if 2 lines intersect
		/// </summary>
		/// <param name="a">first point of the first line</param>
		/// <param name="b">2nd pt of the 2nd line</param>
		/// <param name="c">1st pt of the 2nd line</param>
		/// <param name="t">2nd pt of the 2nd line</param>
		/// <returns>Return true if the 2 lines have an intersection</returns>
		public bool intersectLine(PointL a, PointL b, PointL c, PointL t)
		{//[c,t] intersect [a,b)?
			Vector ab=new Vector(a,b);
			Vector ac=new Vector(a,c);
			Vector at=new Vector(a,t);
			PointL o=new PointL(0,0);
            if (orientation(a,t,c)>=0)
			    return (orientation(a,b,c)!=orientation(a,b,t))&&(orientation(b,a,t)>0)&&(orientation(b,c,t)>0);
            else
                return (orientation(a, b, c) != orientation(a, b, t)) && (orientation(b, a, t) <= 0) && (orientation(b, c, t) <= 0);
		}
        /// <summary>
        /// To know if 2 lines intersect
        /// </summary>
        /// <param name="l1">first line</param>
        /// <param name="l2">second line</param>
        /// <returns>Return true if the 2 lines have an intersection</returns>
        public bool intersectLine(Line l1, Line l2)
        {
            return intersectLine(l1.p1, l1.p2, l2.p1, l2.p2);
        }

        /// <summary>
        /// Get the coordinates of the intersection of 2 straight lines (not segments yet) !!! So only use it if you are sure there is an intersection.
        /// </summary>
        /// <param name="l1">line1</param>
        /// <param name="l2">line2</param>
        /// <returns>Return the point of the intersection of the 2 lines, or null if there is no intersection</returns>
        public PointL intersect(Line l1, Line l2)
        {
            double det = l1.ax * (-l2.ay) + l1.ay * l2.ax;
            if (det != 0)
            {
                double t = (l1.ax * (l2.by - l1.by) - l1.ay * (l2.bx - l1.bx))/det;
                //if ((t > 0) && (t < l2.tf))
                //{
                if ((t < 0) || (t > l2.tf))
                {
                    //Console.WriteLine("**Straight line but not line");
                    return null;
                }
                    double x = l2.ax * t + l2.bx;
                    double y = l2.ay * t + l2.by;
                    return new PointL(x, y);
                    
                //}
                //else
                //    return null;
            }
            else
                return null;
        }

        //public bool isVisible(PointL a, PointL b)
        //{
        //    return false;
        //}
			
        //public bool onTheRight(PointL a, PointL b, PointL c, PointL d, PointL t)
        //{
        //    Vector ab=new Vector(a,b);
        //    Vector bc=new Vector(b,c);
        //    Vector cd=new Vector(c,d);
        //    Vector bt=new Vector(b,t);
        //    Vector ct=new Vector(c,t);
        //    PointL  o =new PointL(0,0);
			
        //    return false;
			
        //}
			
		
	}
}
