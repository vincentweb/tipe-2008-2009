﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using gdiTests;
using dataStruct;
using dataStruct.map;
using geometry;

namespace MovingObject
{
    enum MODE
    {
        OK,//everything is OK
        WRONG,// we need to come back because of a collision with a wall, ...
    }

    class Brain
    {
        public  Mobile           m;
        public  Seeker           seeker;
        public  long             timing;
        public  PointL           dest;
        private Bitmap           bmpMapKnown;
        private Graph            graph;
        private MODE  mode;
        private Line  mLine;
        private int   size;
        private int   prevTime    = 0;
        private int   prevTime2   = 0;
        private int   maxDistance = 100;
        private int   timestamp;
        private int   prevX;
        private int   prevY;
        private bool  startSeeker;
        private bool  collide = true;
        private int   windowHeight;
        private int   windowWidth;
        private int   number;
        private LArray map; //pointeur
        private MOBILE_MODE mobileMode = MOBILE_MODE.MOTION2;

        public Brain(int x, int y, int size, int windowHeight, int windowWidth, int red, int green, int blue, int number, PointL goal, LArray map, bool collide)
        {
            prevX        = x;
            prevY        = y;
            this.collide = collide;
            startSeeker  = true;
            mLine        = new Line(x, y, x + 9.9, y + 9.9);
            m            = new Mobile(prevX, prevY, 5, windowHeight, windowWidth, red, green, blue);
            dest         = new PointL(prevX, prevY, "Dest");
            bmpMapKnown  = new Bitmap(windowWidth / 10 + 1, windowHeight / 10 + 1);
            seeker       = new Seeker(m, map, maxDistance, 50, 0.1, 1, windowWidth, windowHeight, mLine);
            mode         = MODE.OK;
            timing       = 0;
            this.number       = number;
            this.windowHeight = windowHeight;
            this.windowWidth  = windowWidth;
            
            this.map = map;
            this.size = size;
        }

        public int getNumber()
        {
            return number;
        }

        public void setNumber(int i)
        {
            number = i;
        }

        public void setCollide(bool collide)
        {
            this.collide = collide;
            if (collide == true)
                map.delLine(mLine);
            else
            {
                mLine.p1 = new PointL(m.getX() - size, m.getY() - size);
                mLine.p2 = new PointL(m.getX() + size, m.getY() + size);
                map.addLine(mLine);
            }
        }

        //public void updateGoal(PointL goal, LArray map)
        //{
        //    this.goal = goal;
        //    this.map  = map;
        //}

        public void clearBmpMapKnown()
        {
            bmpMapKnown = new Bitmap(windowWidth / 10 + 1, windowHeight / 10 + 1);
        }

        public void clearPath()
        {
            m.pointFixList.Clear();
        }

        public void wander()
        {
            m.wander();
        }

        public void regulator(PointL goal)
        {
            m.goToPoint(goal);
            int timestamp = Int32.Parse(DateTime.Now.ToString("mmssfff"));//game timestamp
            if ((Math.Abs(timestamp - prevTime) > 60) && ((m.getX() != prevX) || (m.getY() != prevY)))
            {
                prevTime = timestamp;
                m.pointFixList.Add2(new PointL(m.getX(), m.getY()), 100);
                prevX = m.getX();
                prevY = m.getY();
            }
        }

        public void motionPlaning(MOBILE_MODE mobileMode)
        {
            this.mobileMode = mobileMode;
            if (mode == MODE.WRONG)
            {// we collide with a wall -> we go to the avant-dernier last good point
                m.goToPoint(m.pointFixList.getLastM());
                if (m.goalReached(m.pointFixList.getLastM()))
                    mode = MODE.OK;
            }
            else
            {
                if (collide == false)
                {
                    mLine.p1 = new PointL(m.getX() - size, m.getY() - size);
                    mLine.p2 = new PointL(m.getX() + size, m.getY() + size);
                    map.moveLine(mLine);
                }
                if (startSeeker == true) //if (Seeker.locker == true)
                {
                    startSeeker = false;
                    seeker.setMobileMode(mobileMode);
                    Thread seekThread = new Thread(new ThreadStart(seeker.run));
                    seekThread.Start();
                }
                timestamp = Int32.Parse(DateTime.Now.ToString("mmssfff"));//game timestamp
                // Print the track

                PointL p = seeker.getDestination();
                if ((p.x != -1) || (p.y != -1))
                {
                    dest.x = p.x;
                    dest.y = p.y;
                    graph = seeker.getGraph();
                }
                if ((dest.x == -3) && (dest.y == -3))
                {
                    mode = MODE.WRONG; //-> on revient sur nos pas
                    startSeeker = true; // to run a new seek
                }
                else
                {
                    if (Math.Abs(timestamp - prevTime) > 60)// && ((m.getX() != prevX) || (m.getY() != prevY)))
                    {
                        prevTime = timestamp;
                        m.pointFixList.Add2(new PointL(m.getX(), m.getY()), 100);
                        prevX = m.getX();
                        prevY = m.getY();
                        timing = seeker.getElaspedTime();
                    }

                    if (((p.x != -1) || (p.y != -1)) && (startSeeker == false))
                    {
                        startSeeker = true;
                    }
                    m.goToPoint(dest);
                }
            }
        }

        public void paintGeneral(Graphics g)
        {
            Pen penPath = new Pen(Color.FromArgb(Math.Min(255,m.red+150),Math.Min(255,m.green+150),Math.Min(255,m.blue+150)));
            Size size = new Size(1, 1);
            foreach (PointL pt in m.pointFixList)
            {
                g.DrawRectangle(penPath, new Rectangle(pt, size));
            }
            m.draw(g);
        }

        public void paintDest(Graphics g)
        {
            g.DrawRectangle(new Pen(Color.Black), new Rectangle(dest, new Size(2, 2)));
        }

        public void paintMotion1(Graphics g)
        {
            Pen pn = new Pen(Color.LightGray, 100);
            //Rectangle rect = new Rectangle(50, 50, x + 50, 100);
            //g.DrawEllipse(pn, rect);
            Size size = new Size(1, 1);
            Size size2 = new Size(2, 2);
            Pen pen2 = new Pen(Color.Green);
            Pen pen3 = new Pen(Color.Red);
            Pen pen5 = new Pen(Color.Black);
            Pen pen6 = new Pen(Color.FromArgb(255, 255, 0));
            Pen penDB = new Pen(Color.DarkBlue, 2F);
            Font font = new Font("Verdana", 10);
            Color BMPpenDB = Color.DarkBlue;

            if (mobileMode == MOBILE_MODE.MOTION2)
            {
                if (graph != null)
                {
                    Pen peng = pen5;
                    // BUG sur le "in", la collection a été modifiée!!
                    int x = 610;
                    g.DrawLine(peng, new Point(470, 470), new Point((int)(470 + 50 * Math.Sin(m.getDirection())), 470 + (int)(50 * Math.Cos(m.getDirection()))));
                    lock (seeker.graphLock)
                    {
                        foreach (KeyValuePair<double, GraphContainer> im in graph.graph)
                        {
                            if (im.Value != null)
                            {
                                if (im.Value.dist == -2)
                                {
                                    //peng = pen6;
                                    peng = new Pen(Color.FromArgb(255, Math.Min(255, Math.Max(16, (int)(255 * im.Value.coef / (15)))), 0));
                                    if (im.Value.coef == -5)
                                        peng = penDB;
                                    try
                                    {
                                        g.DrawRectangle(peng, new Rectangle((int)(470 + 50.5 * Math.Sin(im.Key)), (int)(470 + 0.25 * 202 * Math.Cos(im.Key)), 1, 1));
                                        g.DrawRectangle(peng, new Rectangle(x, 150 - 50, 1, 1));
                                    }
                                    catch (OverflowException)
                                    {
                                        Console.WriteLine("A mathematical operation caused an overflow.");
                                    }
                                }
                                else
                                {
                                    //peng = pen5;
                                    peng = new Pen(Color.FromArgb(255, Math.Min(255, Math.Max(16, (int)(255 * im.Value.coef / (15)))), 0));
                                    if (im.Value.coef == -5)
                                        peng = penDB;
                                    try
                                    {
                                        g.DrawRectangle(peng, new Rectangle((int)(470 + (50.0 / maxDistance) * im.Value.dist * Math.Sin(im.Key)), (int)(470 + (50.0 / maxDistance) * im.Value.dist * Math.Cos(im.Key)), 1, 1));
                                        g.DrawRectangle(peng, new Rectangle(x, 150 - (int)((50.0 / maxDistance) * im.Value.dist), 1, 1));
                                    }
                                    catch (OverflowException)
                                    {
                                        Console.WriteLine("A mathematical operation caused an overflow.");
                                    }
                                }
                            }
                            //map known
                            g.DrawRectangle(penDB, 470, 470, 1, 1);
                            g.DrawImage(bmpMapKnown, new Point(620, 20));
                            g.DrawRectangle(pen5, 620, 20, windowWidth / 10, windowHeight / 10);
                            if (timestamp - prevTime2 > 100)
                            {
                                prevTime2 = timestamp;
                                bool[,] mapKnown = seeker.getMapKnown();
                                for (int i = 0; i < windowWidth / 10; i++)
                                {
                                    for (int j = 0; j < windowHeight / 10; j++)
                                    {
                                        if (mapKnown[i, j] == true)
                                        {
                                            bmpMapKnown.SetPixel(i, j, BMPpenDB);
                                        }
                                    }
                                }
                            }
                            x += 1;
                        }
                    }
                }
            }
            
        }

           
    }
}
