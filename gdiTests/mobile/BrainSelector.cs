﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Text;
using gdiTests;
using dataStruct;
using dataStruct.map;
using geometry;

namespace MovingObject
{
    class BrainSelector
    {
        private ArrayList brains = new ArrayList();
        private Brain newBrain;
        private Brain selected;
        private bool collide=true;

        public BrainSelector()
        {
        }

        public void addBrain(int x, int y, int size, int roomHeight, int roomWidth, int red, int green, int blue, PointL goal, LArray map)
        {
            newBrain = new Brain(x, y, size, roomHeight, roomWidth, red, green, blue, brains.Count, goal, map, collide);
            brains.Add(newBrain);
            if (brains.Count == 1)
                selected = newBrain;
        }

        public void addBrain(int x, int y, int size, int roomHeight, int roomWidth, PointL goal, LArray map)
        {
            Random rnd = new Random();
            // on veut pas que le mobile soit clair (difficile à voir, et trace blanche par translation des couleurs), donc on décide que la somme des couleurs ne doit pas dépasser 255
            int blue  = rnd.Next(0, 256);
            int red   = rnd.Next(0, 256 - blue);
            int green = rnd.Next(0, 256 - red - blue);
            addBrain(x, y, size, roomHeight, roomWidth, red, green, blue, goal, map);
        }

        public void deletedBrain()
        {
            if (brains.Count > 0)
                brains.RemoveAt(selected.getNumber());
            foreach (Brain brain in brains)
            {
                brain.setNumber(brains.IndexOf(brain));
            }
            selectBrain(0);
        }

        public bool findMobile(int x, int y)
        {
            foreach (Brain brain in brains)
            {
                if ((Math.Abs(brain.m.getX() - x) < (6+brain.m.getSize())) && (Math.Abs(brain.m.getY() - y) < (6+brain.m.getSize())))
                {
                    selected = brain;
                    return true;
                }
            }
            return false;
        }

        public Brain getSelectedBrain()
        {
            return selected;
        }

        public void selectBrain(int i)
        {
            foreach (Brain brain in brains)
            {
                if (brain.getNumber() == i)
                    selected = brain;
            }
        }

        public void paintAll(Graphics g)
        {
            foreach (Brain brain in brains)
            {
                brain.paintGeneral(g);
            }
        }

        public void paintDestAll(Graphics g)
        {
            foreach (Brain brain in brains)
            {
                brain.paintDest(g);
            }
        }

        public void paintMotionAll(Graphics g)
        {
            selected.paintMotion1(g);
        }

        public void regulatorAll(PointL goal)
        {
            foreach (Brain brain in brains)
            {
                brain.regulator(goal);
            }
        }

        public void motionAll(MOBILE_MODE mobileMode)
        {
            foreach (Brain brain in brains)
            {
                brain.motionPlaning(mobileMode);
            }
        }

        public void setCollide(bool collide)
        {
            this.collide = collide;
            foreach (Brain brain in brains)
            {
                brain.setCollide(collide);
            }
        }

        //public void updateEnv(PointL goal, LArray map)
        //{// to update the goal point in all the mobile
        //    foreach (Brain brain in brains)
        //    {
        //        brain.updateGoal(goal, map);
        //    }
        //}
    }
}
