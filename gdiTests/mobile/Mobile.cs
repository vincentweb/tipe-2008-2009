﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using gdiTests;
using geometry;
using dataStruct;

namespace MovingObject
{
    partial class Mobile // partial because class split into 2 files
    {
        private int size;              // pixel
        private double x = 10;                 // pixel
        private double y = 10;                 // pixel
        private double direction;       // radian
        private double speed = 0;           // pixel/ms
        private double maxSpeed = 15;        // pixel/ms
        private double maxSteer=0.5;
        private double maxAcceleration = 0.3F; // pixel/ms^2
        private int roomHeight;
        private int roomWidth;
        //private int prevTime=0;
        public  int red = 0;
        public  int green = 0;
        public  int blue = 0;
        public PointLCollection pointFixList;
        public Geometry geo;

        //ANNOYING BUG when goal at 0deg of mobile: mobile make a 360deg turn

        public Mobile(int x, int y, int size, int roomHeight, int roomWidth, int red, int green, int blue)
        {
            this.size        = size;
            this.roomHeight  = roomHeight;
            this.roomWidth   = roomWidth;
            this.x           = x;
            this.y           = y;
            this.red = red;
            this.green = green;
            this.blue = blue;
            pointFixList = new PointLCollection();
            //PointL ppp = new PointL(40, 360);
            //PointL pp  = new PointL(40,380);
            //pointFixList.Add2(ppp, 100);
            //pointFixList.Add2(pp, 100);
            geo = new Geometry();
        }

        public int getX()
        {
            return (int)Math.Round(x,0);
        }
        public int getY()
        {
            return (int)Math.Round(y,0);
        }
        public double getDX()
        {
            return x;
        }
        public double getDY()
        {
            return y;
        }
        public double getSpeed()
        {
            return speed;
        }
        public double getDirection()
        {
            return direction;
        }

        public int getSize()
        {
            return size;
        }

        public double getMaxAcceleration()
        {
            return maxAcceleration;
        }

        public double getMaxSpeed()
        {
            return maxSpeed;
        }

        public void setAcceleration(double value)
        {
            maxAcceleration = value;
        }

        public void setMaxSpeed(double value)
        {
            maxSpeed = value;
        }


        public void move()
        {
            x += (speed * Math.Cos(direction)) % roomHeight;
            y += (speed * Math.Sin(direction)) % roomWidth;
            if (x < 0)
                x = roomHeight;
            if (y < 0)
                y = roomWidth;
            if (x > roomHeight)
                x = 0;
            if (y > roomWidth)
                y = 0;

        }

        public double angle (Point p1,Point p2)
        {
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            return Math.Atan2(dy, dx);
        }

        public double angle(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            double angle = Math.Atan2(dy, dx);
            if (angle < 0)
                angle += 6.28319;
            return angle;
        }
        // A DEPLACER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        public double distance(Point p1, Point p2)
        {
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        public double distance(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            return Math.Sqrt(dx * dx + dy * dy);
        }


        public void accelerate(double rate) //rate between -100 and 100
        {
            speed = Math.Max(0,Math.Min(rate*maxAcceleration/100+speed,maxSpeed));
        }

        public void turn(double steer)
        {// minimum une seconde et demi pour tourner à 90°
            direction += steer;
            if (direction < 0)
                direction += 6.28319;
            if (direction > 6.28319)
                direction -= 6.28319;
        }

        public void draw(Graphics g)
        {
            Pen pen_circle = new Pen(Color.FromArgb(red, green, blue), size);
            Pen pen_line   = new Pen(Color.LightBlue, 1);
            Rectangle rectCircle = new Rectangle(getY(), getX(), size, size);
            g.DrawEllipse(pen_circle, rectCircle);
            g.DrawLine(pen_line, getY() + size / 2, getX() + size / 2, (float)(getY() + size / 2 + size * Math.Sin(direction)), (float)(getX() + size / 2 + size * Math.Cos(direction)));
            

        }

        public void wander()
        {

            //int timestamp = Int32.Parse(DateTime.Now.ToString("mmssfff"));//game timestamp
            //if (timestamp - prevTime > 20)
            //{
                Random rnd = new Random();
                if (rnd.Next(0, 4) < 1)
                    direction += 0.03 * rnd.Next(-2, 3);// Between -2 and 2
                move();
            //    prevTime = timestamp;
            //}
        }

    }
}
