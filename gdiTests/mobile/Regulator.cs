﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using gdiTests;
using geometry;

namespace MovingObject
{
    partial class Mobile
    {
        public void goToSpeed(double s)
        {
            double ds = s - speed;
            double ads = Math.Abs(ds);
            int sign = 1;
            if (ds < 0)
                sign = -1;
            //if (ads < 2)
            //    accelerate(50 * sign * ads);
            //else
            if ((s == 0) && ((ads+0.1) <= maxAcceleration))
            {
                speed = 0;
                //Console.WriteLine("a0");
            }
            else
            {
                accelerate(100 * sign);
                //Console.WriteLine("af");
            }
        }

        public void turnToDirection(double d)
        {
            double dd = d - direction;
            if (dd < -3.14159)
                dd = 6.28319 + dd;
            if (dd > 3.14159)
                dd = -6.28319 + dd;

            //Console.WriteLine("dd {0}", dd);
            double add = Math.Abs(dd);
            int sign = 1;
            if (dd < 0)
                sign = -1;
            if (add < 0.785) //10 deg
                turn(maxSteer / 0.785 * sign * add);
            else
                turn(maxSteer * sign);
        }

        public void manageSpeed(PointL p)
        {
            double d = distance(p.x, p.y, x, y);
            double ang = angle(x, y, p.x, p.y);
            double ecart = Math.Abs(ang - direction); 
            if (ecart >= 3.14159)
                ecart = 6.28319 - ecart;

            if (d < 5)
            {
                goToSpeed(0);
                //Console.WriteLine("0");
            }
            else if (speed == 0)
            {
                goToSpeed(1);
                turnToDirection(ang);
                //Console.WriteLine("1");
            }
            else if (ecart > 0.15)
            {
                goToSpeed(1);
                turnToDirection(ang);
            }
            else if (ecart > 1.5708)
            {
                goToSpeed(0);
                turnToDirection(ang);
            }
            else
            {
                //Console.WriteLine("t");
                int ticks = (int)(Math.Max(0,(d-5)) / speed);// -5 = security distance
                if (ticks > 0)
                {
                    goToSpeed(ticks * maxAcceleration);
                    turnToDirection(ang);
                }
                else
                    goToSpeed(0);
            }
        }

        public bool goalReached(PointL p)
        {
            if (geo.distance(new PointL(x, y), p) < 10)
                return true;
            else
                return false;
        }

        public void goToPoint(PointL p)
        {
            manageSpeed(p);
            //turnToDirection(angle(x, y, p.x, p.y));
            move();
        }
    }
}
