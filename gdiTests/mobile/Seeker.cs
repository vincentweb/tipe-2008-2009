﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MovingObject;
using dataStruct;
using dataStruct.map;
using geometry;
using gdiTests;
using System.Diagnostics;

namespace MovingObject
{
    class Seeker
    {
        Mobile   m;
        LArray   map;
        int      maxVision; // 20 recommended
        double   thetaStep;
        int      cellSize;
        int      visionStep;
        double   minDistAccepted;
        long     tempsExecution = -1;
        int windowWidth;
        int windowHeight;
        PointL   destination;
        Geometry geo      = new Geometry();
        LineToEq lineToeq = new LineToEq();
        public static bool locker   = true;
        Stopwatch chrono;
        public int nb = 0;
        public double maxDistance = 200;
        PointL lastSeekPt;
        Graph graph;
        bool[,] mapKnown;
        double maxCoefTheta = 16;
        double maxCoefKnown = 4;
        double maxCoefDist = 20;
        public object graphLock = new object();
        MOBILE_MODE mobileMode = MOBILE_MODE.MOTION1;
        Log logGraph = new Log("graph.txt");
        Line mLine;
        
        //LE LOCKER NE FONCTIONNE PAS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /// <summary>
        /// Look around the mobile and tell the next point where to go
        /// </summary>
        /// <param name="m">the mobile</param>
        /// <param name="map">the map</param>
        /// <param name="maxVision">longest area</param>
        /// <param name="minDistAccepted">useless?</param>
        /// <param name="thetaStep"></param>
        /// <param name="visionStep"></param>
        /// <param name="windowWidth"></param>
        /// <param name="windowHeight"></param>
        /// <param name="mLine">the line of the mobile when collide is false</param>
        public Seeker (Mobile m, LArray map, int maxVision, double minDistAccepted, double thetaStep, int visionStep, int windowWidth,int windowHeight, Line mLine)
        {
            chrono = new Stopwatch();
            this.maxVision       = maxVision; // cells
            this.thetaStep       = thetaStep;
            this.m               = m;
            this.map             = map;
            this.visionStep      = visionStep;
            this.mLine = mLine;
            cellSize             = map.getCellSize();
            destination = new PointL(-1, -1); //new PointL(m.getX(), m.getY());
            this.minDistAccepted = minDistAccepted;
            this.maxDistance = maxVision;
            this.windowHeight = windowHeight;
            this.windowWidth = windowWidth;
            mapKnown= new bool[windowWidth / 10+1, windowHeight / 10+1];
            for (int i = 0;i<windowWidth / 10;i++)
            {
                for (int j = 0; j < windowHeight/10;j++ )
                {
                    mapKnown[i, j] = false;
                }
            }
        }

        public void run()
        {
            chrono.Reset();
            destination = new PointL(-1, -1);// on renvoie ça si on a pas fini de calculer
            chrono.Start();
            //Thread.Sleep(800);
            if (mobileMode == MOBILE_MODE.MOTION1)
                destination = checkPt();
            else
                destination = check2Pt();
            //logGraph.add("\r\n");
            //Console.WriteLine("\r\n");
            if ((((int)destination.x / cellSize) == ((int)m.getDX() / cellSize)) && (((int)destination.y / cellSize) == ((int)m.getDY() / cellSize)))
            {
                destination = new PointL(-3, -3);
                Console.WriteLine("-------- Collision with wall! -------------");
            }
            lastSeekPt = destination;
            locker      = true; // we start an other thread only the previous fully run
            chrono.Stop();
            tempsExecution = chrono.ElapsedMilliseconds;
            locker = false;
        }

        public void setMobileMode(MOBILE_MODE mobileMode)
        {
            this.mobileMode = mobileMode;
        }

        public PointL getDestination()
        {
            //Console.WriteLine(".. sending {0}", nb);
            return destination;
        }

        public bool[,] getMapKnown()
        {
            return mapKnown;
        }

        public Graph getGraph()
        {
            return graph;
        }

        public long getElaspedTime()
        {
            return tempsExecution;
        }

        public double getMaxCoefTheta()
        {
            return maxCoefTheta;
        }

        public double getMaxCoefDist()
        {
            return maxCoefDist;
        }

        public double getMaxCoefKnown()
        {
            return maxCoefKnown;
        }

        public void eraseMapKnown()
        {
            for (int i = 0; i < windowWidth / 10; i++)
            {
                for (int j = 0; j < windowHeight / 10; j++)
                {
                    mapKnown[i, j] = false;
                }
            }
        }

        public void setMaxCoefTheta(double value)
        {
            maxCoefTheta = value;
        }
        public void setMaxCoefKnown(double value)
        {
            maxCoefKnown = value;
        }
        public void setMaxCoefDist(double value)
        {
            maxCoefDist = value;
        }

        public PointL check2Pt()
        {
            double mx = m.getDX();
            double my = m.getDY();
            double bestCoef = 0;
            PointL bestPointL = new PointL(-1, -1);
            double bestTheta = 0;
            double bestDist = 0;
            PointL destination;
            PointL mPt = new PointL(mx, my);
            //double iDir = geo.angle(mPt, lastSeekPt);
            double iDir = m.getDirection();
            if (iDir < 0)
                iDir += 6.273185;
            graph = new Graph();
            double theta = 0;
            for (theta = iDir; theta < iDir + 6.283185; theta += thetaStep)
            {
                PointL pdest = new PointL(mx + (int)(maxDistance * Math.Cos(theta)), my + (int)(maxDistance * Math.Sin(theta)));
                Line l = new Line(mPt, pdest);
                double d = map.lineDist(l,mx,my, ref mapKnown, maxDistance, mLine);
                double coef = maxCoefTheta;
                coef += d * maxCoefDist / maxDistance;
                destination = new PointL(d * Math.Cos(theta) + m.getDX(), d * Math.Sin(theta) + m.getDY());
                if ((destination.dx < 0) || (destination.dy < 0) || (((int)destination.dx / cellSize) >= mapKnown.GetLength(1)) || (((int)destination.dy / cellSize) >= mapKnown.GetLength(0)))
                {
                    Console.WriteLine("DEST IS OUT!!!!!");
                    coef = 0;
                }
                else
                {
                    if (d > maxDistance)
                    {
                        //Console.WriteLine("!! too far  {0}", d);
                        //d = -2;
                        d = maxDistance + 1;
                    }
                    if (mapKnown[((int)destination.dy / cellSize), ((int)destination.dx / cellSize)] == false)
                        coef += maxCoefKnown;
                    if (theta < 3.14159 + iDir)
                        coef -= maxCoefTheta * (theta - iDir) / 3.14159;
                    else
                        coef -= maxCoefTheta * (6.283185 - theta + iDir) / 3.14159;

                    //logGraph.add(theta + ":" + d + "\r\n");
                    //Console.WriteLine(theta + ":" + d);

                    if (bestCoef < coef)
                    {
                        bestCoef = coef;
                        bestPointL = destination;//pdest
                        bestTheta = theta;
                        bestDist = d;
                    }

                    try
                    {
                        GraphContainer gc = new GraphContainer(d, coef);
                        lock (graphLock)
                        {
                            graph.graph[theta] = gc;//distance
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Impossible to add data in graph.graph");
                    }
                }
            }
            try
            {
                GraphContainer gcc = new GraphContainer(graph.graph[bestTheta].dist, -5);
                graph.graph[bestTheta] = gcc;
            }
            catch (KeyNotFoundException e)
            {
                Console.WriteLine("ERROR SEEKER MOTION 2 KEY NOT FOUND");
            }
            return bestPointL;
        }
        
        public PointL checkPt()
        {
            PointL pm = new PointL(m.getX(), m.getY());
            for (int dist = maxVision; dist > 0; dist -= visionStep)
            {
                PointL pdest = new PointL(m.getX()+(int)(dist * Math.Cos(m.getDirection())), m.getY()+(int)(dist * Math.Sin(m.getDirection())));
                Line l = new Line(pm, pdest);// line between the mobile and the point "dest"
                PointL p = map.lineIntersect(l, m.getX(), m.getY(), mLine);
                //Console.WriteLine(geo.distance(m.getX(), m.getY(), p.x, p.y));
                Random rand = new Random();
                int epsilon = -1;
                if ( (p.x == -1) && (p.y == -1) )
                    return pdest;// this position is free, we go there, we don't turn the head
                if (geo.distance(m.getX(), m.getY(), p.x * cellSize, p.y * cellSize) >= minDistAccepted)
                {// the position is not free, but there is a free position far enough
                    pdest.x = p.x * cellSize + cellSize / 2;
                    pdest.y = p.y * cellSize + cellSize / 2;
                    return pdest;
                }
                for (double theta = thetaStep; theta < 3.14159; theta += thetaStep)// we start at thetaStemp coz theta=0 already checked
                {
                    epsilon = 2*rand.Next(0,2)-1;// turn the head first on the left or on the right
                    pdest = new PointL(m.getX() + (int)(dist * Math.Cos(m.getDirection() + epsilon * theta)), m.getY() + (int)(dist * Math.Sin(m.getDirection() + epsilon * theta)));
                    l = new Line(pm, pdest);
                    p = map.lineIntersect(l, m.getX(), m.getY(), mLine);
                    if ((p.x == -1) && (p.y == -1))
                        return pdest;// free position, let's go
                    if (geo.distance(m.getX(), m.getY(), p.x * cellSize, p.y * cellSize) >= minDistAccepted)
                    {// far enough, let's go
                        pdest.x = p.x * cellSize + cellSize / 2;
                        pdest.y = p.y * cellSize + cellSize / 2;
                        return pdest;
                    }
                    // same for the opposite side
                    pdest = new PointL(m.getX() + (int)(dist * Math.Cos(m.getDirection() - epsilon * theta)), m.getY() + (int)(dist * Math.Sin(m.getDirection() - epsilon * theta)));
                    l = new Line(pm, pdest);
                    p = map.lineIntersect(l, m.getX(), m.getY(), mLine);
                    if ((p.x == -1) && (p.y == -1))
                        return pdest;
                    if (geo.distance(m.getX(), m.getY(), p.x * cellSize, p.y * cellSize) >= minDistAccepted)
                    {
                        pdest.x = p.x * cellSize + cellSize / 2;
                        pdest.y = p.y * cellSize + cellSize / 2;
                        return pdest;
                    }
                }
            }
            return new PointL(m.getX(), m.getY());
        }

       
    }
}
