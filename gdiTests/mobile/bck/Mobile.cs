﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using gdiTests;
namespace MovingObject
{
    partial class Mobile
    {
        private int size;              // pixel
        private int updateDelay;       // ms
        private double x = 10;                 // pixel
        private double y = 10;                 // pixel
        private double direction;       // radian
        private double speed = 0;           // pixel/ms
        private double maxSpeed = 7;        // pixel/ms
        private double maxSteer=0.5;
        private double maxAcceleration = 0.3F; // pixel/ms^2
        private int roomHeight;
        private int roomWidth;
        private int prevTime=0;

        //ANNOYING BUG when goal at 0deg of mobile: mobile make a 360deg turn

        public Mobile(int x, int y, int size, int updateDelay, int roomHeight, int roomWidth)
        {
            this.size        = size;
            this.updateDelay = updateDelay;
            this.roomHeight  = roomHeight;
            this.roomWidth   = roomWidth;
            this.x           = x;
            this.y           = y;
        }

        public int getX()
        {
            return (int)Math.Round(x,0);
        }
        public int getY()
        {
            return (int)Math.Round(y,0);
        }
        public double getDX()
        {
            return x;
        }
        public double getDY()
        {
            return y;
        }
        public double getSpeed()
        {
            return speed;
        }
        public double getDirection()
        {
            return direction;
        }


        public void move()
        {
            x += (speed * Math.Cos(direction)) % roomHeight;
            y += (speed * Math.Sin(direction)) % roomWidth;
            if (x < 0)
                x = roomHeight;
            if (y < 0)
                y = roomWidth;
            if (x > roomHeight)
                x = 0;
            if (y > roomWidth)
                y = 0;

        }

        public double angle (Point p1,Point p2)
        {
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            return Math.Atan2(dy, dx);
        }

        public double angle(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            double angle = Math.Atan2(dy, dx);
            if (angle < 0)
                angle += 6.28319;
            return angle;
        }
        // A DEPLACER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        public double distance(Point p1, Point p2)
        {
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        public double distance(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            return Math.Sqrt(dx * dx + dy * dy);
        }


        public void accelerate(double rate) //rate between -100 and 100
        {
            speed = Math.Min(rate*maxAcceleration/100+speed,maxSpeed);
        }

        public void turn(double steer)
        {// minimum une seconde et demi pour tourner à 90°
            direction += steer;
            if (direction < 0)
                direction += 6.28319;
            if (direction > 6.28319)
                direction -= 6.28319;
        }

        public void draw(Graphics g)
        {
            Pen pen_circle = new Pen(Color.DarkBlue, size);
            Pen pen_line   = new Pen(Color.LightBlue, 1);
            Rectangle rectCircle = new Rectangle(getY(), getX(), size, size);
            g.DrawEllipse(pen_circle, rectCircle);
            g.DrawLine(pen_line, getY() + size / 2, getX() + size / 2, (float)(getY() + size / 2 + size * Math.Sin(direction)), (float)(getX() + size / 2 + size * Math.Cos(direction)));
            

        }

        public void wander()
        {

            //int timestamp = Int32.Parse(DateTime.Now.ToString("mmssfff"));//game timestamp
            //if (timestamp - prevTime > 20)
            //{
                Random rnd = new Random();
                if (rnd.Next(0, 4) < 1)
                    direction += 0.03 * rnd.Next(-2, 3);// Between -2 and 2
                move();
            //    prevTime = timestamp;
            //}
        }

    }
}
