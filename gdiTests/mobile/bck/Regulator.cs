﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using gdiTests;

namespace MovingObject
{
    partial class Mobile
    {
        public void goToSpeed(double s)
        {
            double ds = s - speed;
            double ads = Math.Abs(ds);
            int sign = 1;
            if (ds < 0)
                sign = -1;
            if (ads < 2)
                accelerate(50 * sign * ads);
            else
                accelerate(100 * sign);
        }

        public void turnToDirection(double d)
        {
            double dd = d - direction;
            if (dd < -3.14159)
                dd = 6.28319 + dd;
            if (dd > 3.14159)
                dd = -6.28319 + dd;

            //Console.WriteLine("dd {0}", dd);
            double add = Math.Abs(dd);
            int sign = 1;
            if (dd < 0)
                sign = -1;
            if (add < 0.785) //10 deg
                turn(maxSteer / 0.785 * sign * add);
            else
                turn(maxSteer * sign);
        }

        public void manageSpeed(PointL p)
        {
            double d = distance(p.x, p.y, x, y);
            double speed1 = this.speed;
            if ((d < 130) && (d > 5))
                speed1 = d * d*maxSpeed / (130*130);
            else if (d < 5)
                speed1 = 0;
            else
                speed1 = maxSpeed;
            double ang = angle(x, y, p.x, p.y);
            double speed2 = speed1;
            double ecart = Math.Abs(ang-direction);
            if (ecart >= 3.14159)
                ecart = 6.28319 - ecart;
            if (ecart > 0.15)
                speed2 = 1;
            if (ecart > 1.5708)
                speed2 = 0;
            //Console.WriteLine(ecart);
            goToSpeed(Math.Min(speed1, speed2));
            turnToDirection(ang);
        }

        public void goToPoint(PointL p)
        {
            manageSpeed(p);
            //turnToDirection(angle(x, y, p.x, p.y));
            move();
        }
    }
}
